# Outlook Automation

A collection of scripts to automate Microsoft Outlook in an office environment.

## Contents

- `DelayOutgoingEveningMail.cls`: Checks for every sent e-mail if it is sent late in the evening/night. If that's the case, it delays sending it until the following morning.

## Install

`.cls` files should be copy pasted into `ThisOutLookSession` using the VBA editor of Outlook (open with <kbd>ALT</kbd>+<kbd>F11</kbd>).
