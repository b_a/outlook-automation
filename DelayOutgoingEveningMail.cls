Private Sub Application_ItemSend(ByVal Item As Object, ByRef Cancel As Boolean)
    Const morningTime As Integer = 7  ' Start of day, mails are delayed until this hour
    Const eveningTime As Integer = 20  ' End of day, mails are delayed after this hour
    Const magicString as String = "```"  ' Mail is not delayed if the body contains this string

    Dim mi As Outlook.MailItem
    Dim evening As Boolean
    Dim night As Boolean
    Dim itIsLate As Boolean
    Dim isNotException As Boolean
    Dim rndMinute As String

    On Error GoTo ErrorHandler

    ' Only add delay to MailItems, not MeetingItems
    If TypeName(Item) = "MailItem" Then
        Set mi = Item
    
        evening = (DatePart("h", Now) > eveningTime)
        night = (DatePart("h", Now) < morningTime)
        itIsLate = (evening Or night)
        rndMinute = CStr(Int((40 - 12 + 1) * Rnd + 12))   ' Random generated minute
        isNotException = (InStr(mi.Body, magicString) = 0) ' Skip delay if the magic string is found
        
        If itIsLate And isNotException Then
            '  in the evening, delay until next morning
            If evening Then
                mi.DeferredDeliveryTime = (Date + 1) & " 0" & morningTime & ":" & rndMinute & ":00"
            ' in the night delay until upcoming morning
            ElseIf night Then
                mi.DeferredDeliveryTime = (Date) & " 0" & morningTime & ":" & rndMinute & ":00"
            End If
            MsgBox "Message will not be sent before: " & mi.DeferredDeliveryTime
        End If
    End If
Exit Sub

ErrorHandler:
    MsgBox "Application_ItemSend: " & Err.Description
End Sub